﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FolderStructurer.Models
{
    public class ReportService
    {
        public FileStream CreateReport(FolderStructurDto folderStructur)
        {
            var wb = new XLWorkbook("../../../Folder_Template.xlsx");

            CreateFileBack(folderStructur, wb);
            CreateContentSite(folderStructur, wb);

            //CreateSeperators(new string[] { "worksheetName", "name2" }, folderStructur.Seperators);
            folderStructur.Seperators = folderStructur.Seperators.OrderBy(p => p.Index).ToList();

            foreach (var seperator in folderStructur.Seperators)
            {
                wb.Worksheet("TS1").CopyTo(seperator.Titele);
                var seperatorWorkSheet = wb.Worksheet(seperator.Titele);
                
                seperatorWorkSheet.Cell("A4").Value = seperator.Titele;
                seperatorWorkSheet.Cell("A5").Value = seperator.Description;
                seperatorWorkSheet.Cell("B1").Value = seperator.Titele;

                seperator.Children = seperator.Children.OrderBy(p => p.Index).ToList();
                foreach (var seperatorChild1 in seperator.Children)
                {
                    wb.Worksheet("TS1").CopyTo(seperatorChild1.Titele);
                    var seperatorChild1WorkSheet = wb.Worksheet(seperatorChild1.Titele);

                    seperatorWorkSheet.Cell("A5").Value = seperator.Titele;
                    seperatorWorkSheet.Cell("A7").Value = seperatorChild1.Titele;
                    seperatorWorkSheet.Cell("A8").Value = seperatorChild1.Description;
                    //cellen einrücken??
                    //seperatorWorkSheet.Cell("A8").Style.Alignment.Indent = 7;
                    seperatorChild1.Children = seperatorChild1.Children.OrderBy(p => p.Index).ToList();
                    foreach (var seperatorChild2 in seperatorChild1.Children)
                    {
                        seperatorChild2.Children = seperatorChild2.Children.OrderBy(p => p.Index).ToList();
                        foreach (var seperatorChild3 in seperatorChild2.Children)
                        {


                        }

                    }

                }
            }

            return null;
        }

        //private void CreateSeperators(string[] ordertWorksheetNames, IList<FolderseparatorDto> seperators)
        //{
        //    if (ordertWorksheetNames.Length == 0)
        //    {
        //        return;
        //    }

        //    foreach (var seperator in seperators)
        //    {

        //    }
        //}

        private void CreateFileBack(FolderStructurDto folderStructur, XLWorkbook wb)
        {
            
        }

        private void CreateContentSite(FolderStructurDto folderStructur, XLWorkbook wb)
        {
            throw new NotImplementedException();
        }
    }
}
