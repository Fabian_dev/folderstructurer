﻿using System;
using System.Collections.Generic;

namespace FolderStructurer.Models
{
    public class FolderseparatorDto
    {
        public int Index { get; set; }

        public string Titele { get; set; }

        public string Description { get; set; }

        public DateTime CreationTime { get; set; }

        public IList<FolderseparatorDto> Children { get; set; }
    }
}