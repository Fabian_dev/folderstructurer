﻿using System.Collections.Generic;

namespace FolderStructurer.Models
{
    public class FolderStructurDto
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public List<FolderseparatorDto> Seperators { get; set; }

        public FolderStructurDto()
        {
            Seperators = new List<FolderseparatorDto>();
        }
    }
    
}