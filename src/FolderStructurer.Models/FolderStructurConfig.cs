﻿using System.Collections.Generic;

namespace FolderStructurer.Models
{
    public class FolderStructurConfig
    {
        public FolderBackConfig BackBroadConfig { get; set; }

        public FolderBackConfig BackSlimConfig { get; set; }

        public List<FolderseparatorConfig> SeperatorConfigs { get; set; }

        public FolderContentConfig ContentConfig { get; set; }

        public FolderStructurConfig()
        {
            SeperatorConfigs = new List<FolderseparatorConfig>();
        }
    }

    public class FolderContentConfig
    {
        public string FirstContentCellName { get; set; }

        public string WorkSheetName { get; set; }
    }

    public class FolderBackConfig
    {
        public string WorkSheetName { get; set; }

        public string TitleCellName { get; set; }

        public string YearCellName { get; set; }

        public string ContentCellName { get; set; }
    }

    public class FolderseparatorConfig
    {
        public string WorkSheetName { get; set; }

        public int Level { get; set; }

        public List<FolderSeperatorReplacementValues> Replacer { get; set; }
    }

    public class FolderSeperatorReplacementValues
    {
        public string[] TitleCellNames { get; set; }

        public string DescriptionCellName { get; set; }
    }
}